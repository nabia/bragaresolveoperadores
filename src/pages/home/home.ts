import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { LoginPage } from '../login/login'
import { OccurrenceslistPage } from '../occurrenceslist/occurrenceslist';
import { Network } from '@ionic-native/network';
import { UserservicesProvider } from '../../providers/userservices/userservices';

declare var require: any; 
var firebase = require("firebase"); 
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  constructor(public navCtrl: NavController, public network: Network, public us: UserservicesProvider) {

  }
  navigateToLogin(){
    var that = this;
//    that.navCtrl.setRoot(LoginPage);
    if(this.network.type){
      var authListenner =  firebase.auth().onAuthStateChanged(function(user) {
        if (user) {
          var x = that;
          that.us.reloadInfo(
            function(){
              x.navCtrl.setRoot(OccurrenceslistPage);
            },
            function(){
              x.navCtrl.setRoot(LoginPage);
            },
          );
        } else {
          // No user is signed in.
          console.log("NO AUTH BRO")
          that.navCtrl.setRoot(LoginPage);
        }
      });    
    }else{
      alert("O dispositivo não tem ligação à internet.");
    }
  }

}
