import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';
import { Modal, ModalController, ModalOptions } from 'ionic-angular';
import { AssignoperatorPage } from '../assignoperator/assignoperator';
import { DuplicatemodalPage } from '../duplicatemodal/duplicatemodal';
import { IgnoremodalPage } from '../ignoremodal/ignoremodal';
import { UserservicesProvider } from '../../providers/userservices/userservices';
import { FirebaseProvider } from '../../providers/firebase/firebase';
import { OccurrenceservicesProvider } from '../../providers/occurrenceservices/occurrenceservices';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { NotificationsserviceProvider } from '../../providers/notificationsservice/notificationsservice';
/**
 * Generated class for the OccurrencePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
declare var require: any; 
var firebase = require("firebase"); 
declare var window: any;
@IonicPage()
@Component({
  selector: 'page-occurrence',
  templateUrl: 'occurrence.html',
}) 
export class OccurrencePage {
  occurrenceView: boolean = true;
  occurrence: any;
  occurrencesList: any[];
  operators: any[] = [];
  accountId: string;
  user: any;
  states: any[] = [
    {
      id: 0,
      text: "Por resolver"
    },
    {
      id: 1,
      text: "Atribuída para resolução"
    },
    {
      id: 2,
      text: "Aguarda validação"
    },
    {
      id: 3,
      text: "Resolvida"
    }
  ];
  interventionNote: string;
  storeId: string = "289549584765897";
  customStyle: any;
  constructor(public ns: NotificationsserviceProvider, public navCtrl: NavController, public navParams: NavParams, public modalCtrl: ModalController, public fb: FirebaseProvider, public us: UserservicesProvider, public op: OccurrenceservicesProvider, public camera: Camera, public platform: Platform) {
    this.occurrence = navParams.get("occurrence");
    this.occurrencesList = navParams.get("occurrencesList");
    this.user = this.us.getUser();
    this.accountId = this.us.getAccountId();
    this.customStyle = [{
      featureType: "poi.business",
      elementType: "labels",
      stylers: [{ visibility: "off" }]
    }];
    console.log(this.occurrence)
    if(!this.occurrence.owner || this.occurrence.owner == undefined){
      this.fb.getOperators().snapshotChanges().map(changes => {
        return changes.map(c => ({ key: c.payload.key, ...c.payload.val() }));
      }).subscribe(result => {
        for(var i = 0; i < result.length; i++){
          if(result[i].activated){
            this.operators.push(
              {id: result[i]["key"], text: result[i]["firstName"]+" "+result[i]["lastName"]}
            );
          }
        }
      });
    } 
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OccurrencePage');
  }

  goBack(){
    this.navCtrl.pop();
  }

  displayOccurrence(){
    this.occurrenceView = true;
  }

  displayIntervention(){ 
    this.occurrenceView = false;
  }

  openOperatorModal(){
    if(!this.occurrence.owner){
      const myModal: Modal = this.modalCtrl.create('AssignoperatorPage', { 
        operatorsList: this.operators
      });
      myModal.present();
      myModal.onDidDismiss((details) => {
        if(details){
          var occurrence = this.occurrence;
          occurrence.owner = details.id;
          occurrence.ownerName = details.name;
          if(details.comment){
            occurrence.operatorComment = details.comment;
          }
          if(occurrence.state <= 0){
            occurrence.state = 1;
            occurrence.initDate = (new Date()).getTime();
          }
          this.op.saveOccurrence(occurrence, "Atribuíção de operador: "+ details.name);
          this.ns.assignOccurrence(details.id);
        }
      });
    }
  }

  openDuplicateModal(){
    const myModal: Modal = this.modalCtrl.create('DuplicatemodalPage', { 
      occurrences: this.occurrencesList
     });
    myModal.present();
    myModal.onDidDismiss((details) => {
      if(details){
        var tOccur = details.occurrence, dOccur = this.occurrence;
        if(!tOccur.duplicates){
          tOccur.duplicates = [];
        }
        tOccur.duplicates.push(this.occurrence.key);
        dOccur.state = -1;
        dOccur.duplicateOf = details.occurrence.key;
    
        this.op.saveOccurrence(tOccur, "Adicionada ocorrência duplicada");
        this.op.saveOccurrence(dOccur, "Ocorrência marcada como duplicada");
      }
    });  
  }

  openIgnoreModal(){
    const myModal: Modal = this.modalCtrl.create('IgnoremodalPage', { occurrenceId: this.occurrence.key });
    myModal.present();
    myModal.onDidDismiss((details) => {
      if(details){
        var occurrence = this.occurrence;
        occurrence.state = -2;
        occurrence.ignored = {
          reason: details.ignoreComment,
          user: this.accountId,
          timestamp: (new Date()).getTime()
        }
    
        this.op.saveOccurrence(occurrence, "Ocorrência ignorada");
      }
    });
  }

  stringToNumber(str){
    return Number(str);
  }

  stateOnChange(ev){
    var occurrence = this.occurrence;
    var newState = parseInt(ev)
    occurrence.state = newState;
    if(newState == 1 || !occurrence.initDate){
      occurrence.initDate = (new Date()).getTime();
    }else if(newState >= 2){
      occurrence.endDate = (new Date()).getTime();
    }
    this.op.saveOccurrence(occurrence, "Mudança de estado: "+ this.states[parseInt(ev)]);
    if(occurrence.public && occurrence.state == 3){
      this.ns.finishNotification(occurrence.createdBy, occurrence.origin);
    }
  }

  recoverOccurrence(){
    var occurrence = this.occurrence;
    switch(occurrence.state){
      case -2: //ignored
        occurrence.ignore = null;
        occurrence.state = 0;
        this.op.saveOccurrence(occurrence, "Mudança de estado: "+ this.states[0]);
        break;
      case -1: //duplicated
        if(!occurrence.duplicateOf) return;
        var duplicateOf = occurrence.duplicateOf;
        occurrence.duplicateOf = null;
        occurrence.state = 0;
        this.op.saveOccurrence(occurrence, "Mudança de estado: "+this.states[0]);
        this.fb.getOccurrence(duplicateOf).valueChanges().subscribe(result => {
          result.key = duplicateOf;
          var occ = result;
          this.updateMainOccurrence(occ);
        });
        break;
      default: break;
    }
  }

  updateMainOccurrence(mainOccurrence){
    if(!mainOccurrence["duplicates"]) return;
    var index = mainOccurrence["duplicates"].indexOf(this.occurrence.key);
    if (index > -1) {
      mainOccurrence["duplicates"].splice(index, 1);
      this.op.saveOccurrence(mainOccurrence, "Duplicado removido.");
    }
  }

  changeVisilibity(isPublic){
    var occurrence = this.occurrence;
    occurrence.public = !!isPublic;
    this.op.saveOccurrence(occurrence, "Visibilidade da ocorrência alterada.");
    if(occurrence.public && occurrence.state == 3){
      this.ns.finishNotification(occurrence.createdBy, occurrence.origin);
    }
  }

  addInterventionNote(){
    var occurrence = this.occurrence;
    var now = (new Date()).getTime()
    var intervention = {
      note: this.interventionNote,
      operator: this.accountId,
      timestamp: now
    }
    occurrence.intervention = intervention;
    if(!occurrence.initDate){
      occurrence.initDate = now;
    }
    if(!occurrence.endDate){
      occurrence.endDate = now;
    }
    occurrence.state = 3;
    this.op.saveOccurrence(occurrence, "Nota de encerramento adicionada. Estado alterado para resolvida.")
  }

  addPic(isAfter){
    const options: CameraOptions = {
      quality: 30,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }

    if(this.platform.is("android")){
      options.destinationType = this.camera.DestinationType.DATA_URL;
    }
    var that = this, _isAfter = isAfter;
    this.camera.getPicture(options).then((imageData) => {
      this.upload(undefined, function(url){
        var occurrence = that.occurrence;
        if(_isAfter){
          occurrence.afterPic = url;
          that.op.saveOccurrence(occurrence, "Foto antes adicionada.")
        }else{
          occurrence.beforePic = url;
          that.op.saveOccurrence(occurrence, "Foto depois adicionada.")
        }
      },imageData);
    }, (err) => {
     console.log("Something wrong happened.")
    });
  }

  removePic(isAfter){
    if(isAfter){
      var ref = firebase.storage().refFromURL(this.occurrence.afterPic);
    }else{
      var ref = firebase.storage().refFromURL(this.occurrence.beforePic);
    }
    var that = this, _isAfter = isAfter;
    ref.delete().then(function() {
      var occurrence = that.occurrence;
      if(_isAfter){
        occurrence.afterPic = null;
        that.op.saveOccurrence(occurrence, "Foto antes removida.")
      }else{
        occurrence.beforePic = null;
        that.op.saveOccurrence(occurrence, "Foto depois removida.")
      }
    }).catch(function(error) {
      //do nothing, nothing is lost
    });
  }

  upload(hasDate,cb,image) {
    console.log(image)
    let storageRef = firebase.storage().ref();
    let success = false;
    let date: Date = hasDate != undefined? hasDate : new Date();
    var constructedDate = date.getFullYear().toString() + (date.getMonth()+1).toString() + date.getDate().toString();
    let contentFolder = "images"
    let elem_id = "file";

    let prefixOne = this.storeId;
    let prefixTwo = constructedDate;
    var name = (new Date()).getTime()+".jpeg";
    let path = `/occurrences/${contentFolder}/${prefixTwo}/${prefixOne}/${name}`;
    var iRef = storageRef.child(path);

    if(this.platform.is("android")){
      iRef.putString(image, 'base64', {contentType:'image/jpeg'}).then((snapshot) => {
        cb(snapshot.downloadURL);
      }, (error) => {
        console.log(JSON.stringify(error))
      });
    }else{
      window.resolveLocalFileSystemURL(image, (fileEntry) => {
        fileEntry.file((resFile) => {
          var reader = new FileReader();
          reader.onloadend = (evt: any) => {
            var imgBlob: any = new Blob([evt.target.result], { type: 'image/jpeg' });
            iRef.put(imgBlob).then((snapshot) => {
              cb(snapshot.downloadURL);
            });
          };
          reader.onerror = (e) => {
            console.log("Failed file read: " + e.toString());
          };
          reader.readAsArrayBuffer(resFile);

        });
      });
    }
  }
}
