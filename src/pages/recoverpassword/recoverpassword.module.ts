import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RecoverpasswordPage } from './recoverpassword';

@NgModule({
  declarations: [
    RecoverpasswordPage,
  ],
  imports: [
    IonicPageModule.forChild(RecoverpasswordPage),
  ],
})
export class RecoverpasswordPageModule {}
