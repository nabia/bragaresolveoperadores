import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UserservicesProvider } from '../../providers/userservices/userservices';
import { FirebaseProvider } from '../../providers/firebase/firebase';
import { Modal, ModalController, ModalOptions } from 'ionic-angular';
import { RecoverpasswordPage } from '../recoverpassword/recoverpassword';
import { OccurrenceslistPage } from '../occurrenceslist/occurrenceslist';
/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
declare var require: any; 
var firebase = require("firebase"); 
declare var FirebasePlugin: any;
@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})


export class LoginPage {
  login: any;
  loginFailedCover: boolean = false;
  constructor(public navCtrl: NavController, public navParams: NavParams,  public fbuilder: FormBuilder, public us: UserservicesProvider, private fb: FirebaseProvider, public modalCtrl: ModalController, public menu: MenuController) {
    this.login = fbuilder.group({
      'email' : [null,Validators.compose([Validators.required, Validators.email])],
      'password' : [null,Validators.compose([Validators.required])]
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }


  submitForm(ev, value){
    ev.preventDefault();
    for (let c in this.login.controls) {
        this.login.controls[c].markAsTouched();
    }
    if (this.login.valid) {
      var that = this;
      console.log("auth")
      firebase.auth().signInWithEmailAndPassword(value.email,value.password)
      .then(function(user){
        var uid = user.uid;
        console.log(user.uid);
        var items = that.fb.getOperator(user.uid).snapshotChanges().map(changes => {
          return changes.map(c => ({ key: c.payload.key, ...c.payload.val() }));
        });
        items.subscribe(userDB => {
            console.log(JSON.stringify(userDB));
            if(!userDB[0]){
              that.loginFailedCover = true;
            }else{
              that.us.setUser(userDB[0], userDB[0].key);
              FirebasePlugin.getToken(token => {
                if(token && token != ""){
                  that.fb.updateDeviceToken(userDB[0].key, token);
                }
                FirebasePlugin.subscribe('all');
              }, error => {
                console.error(`Error: ${error}`);
              });
              FirebasePlugin.onTokenRefresh(token => {
                if(token && token != ""){
                  that.fb.updateDeviceToken(userDB[0].key, token);
                }
              }, function(error) {
                console.error(`Error: ${error}`);
              });
              that.goToList();
            }
        });
      })
      .catch(function(error) {
        console.log("entrou no catch")
        console.log(JSON.stringify(error));
        that.loginFailedCover = true;
      });
    }
  }

  goToList(){
    this.menu.enable(false);
    this.navCtrl.setRoot(OccurrenceslistPage);
  }

  dismissCover(){
    this.loginFailedCover = false;
  }

  recoverPassword(){
    //occurrence as argument
    const myModal: Modal = this.modalCtrl.create('RecoverpasswordPage', { data: {} });

    myModal.present();

    myModal.onDidDismiss((data) => { 
    });
  }

}
