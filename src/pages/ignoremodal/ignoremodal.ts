import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ViewController } from 'ionic-angular';
import { OccurrenceservicesProvider } from '../../providers/occurrenceservices/occurrenceservices';
/**
 * Generated class for the IgnoremodalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-ignoremodal',
  templateUrl: 'ignoremodal.html',
})
export class IgnoremodalPage {
  ignoreComment: string;
  occurrenceId: string;
  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, public op: OccurrenceservicesProvider) {
    this.occurrenceId = navParams.get("occurrenceId");
  }

  ionViewDidLoad() { 
    console.log('ionViewDidLoad IgnoremodalPage');
  }

  public backModal(){
    this.viewCtrl.dismiss(); 
  }

  public closeModal(){ 
    this.viewCtrl.dismiss({
      ignoreComment: this.ignoreComment
    }); 
  }
}
