import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { IgnoremodalPage } from './ignoremodal';

@NgModule({
  declarations: [
    IgnoremodalPage,
  ],
  imports: [
    IonicPageModule.forChild(IgnoremodalPage),
  ],
})
export class IgnoremodalPageModule {}
