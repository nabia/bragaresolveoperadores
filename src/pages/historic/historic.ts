import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController } from 'ionic-angular';
import { MapPage } from '../map/map'
import { OccurrenceslistPage } from '../occurrenceslist/occurrenceslist'
import { ReportPage } from '../report/report'
import { OccurrencePage } from '../occurrence/occurrence'
import { UserservicesProvider } from '../../providers/userservices/userservices';
import { FirebaseProvider } from '../../providers/firebase/firebase';
import { OccurrenceservicesProvider } from '../../providers/occurrenceservices/occurrenceservices';
/**
 * Generated class for the HistoricPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-historic',
  templateUrl: 'historic.html',
})
export class HistoricPage {
  user: any;
  accountId: string;
  occurrences: any[];
  constructor(public navCtrl: NavController, public navParams: NavParams, public menu: MenuController, public us: UserservicesProvider, public fb: FirebaseProvider, public op: OccurrenceservicesProvider) {
    this.user = this.us.getUser();
    this.accountId = this.us.getAccountId();
    this.fb.getOccurrences().snapshotChanges().map(changes => {
      return changes.map(c => ({ key: c.payload.key, ...c.payload.val() }));
    }).subscribe(result => {
      var occur = [];
      for(var i = 0; i < result.length; i++){
        occur.unshift(result[i]);
      }
      this.occurrences = occur;
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HistoricPage');
  }

  navigateToReport(){
    this.menu.enable(false);
    this.navCtrl.setRoot(ReportPage);
  }

  navigateToMap(){
    this.menu.enable(false);
    this.navCtrl.setRoot(MapPage);
  }

  navigateToList(){
    this.menu.enable(false);
    this.navCtrl.setRoot(OccurrenceslistPage);
  }

  navigateToOccurrence(occurrence){
    this.navCtrl.push(OccurrencePage, {
      occurrence: occurrence,
      occurrencesList: this.occurrences
    })
  }

  logout(){
    this.us.logout();
  }
}
