import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AssignoperatorPage } from './assignoperator';

@NgModule({
  declarations: [
    AssignoperatorPage,
  ],
  imports: [
    IonicPageModule.forChild(AssignoperatorPage),
  ],
})
export class AssignoperatorPageModule {}
