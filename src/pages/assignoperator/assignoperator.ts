import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ViewController } from 'ionic-angular';
import { UserservicesProvider } from '../../providers/userservices/userservices';
/**
 * Generated class for the AssignoperatorPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-assignoperator',
  templateUrl: 'assignoperator.html',
})
export class AssignoperatorPage {
  selectedOperator: string = null;
  selectedOperatorName: string;
  operators: any[];
  operatorComment: string;
  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, public us: UserservicesProvider) {
    this.operators = navParams.get("operatorsList");
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AssignoperatorPage');
  }
  public backModal(){
    this.viewCtrl.dismiss(); 
  }

  public closeModal(){
    this.viewCtrl.dismiss({
      id: this.selectedOperator,
      name: this.selectedOperatorName,
      comment: this.operatorComment
    }); 
  }

  selectOperator(operator){
    this.selectedOperatorName = operator.text;
    this.selectedOperator = operator.id;
  }

  isSelected(operator){
    return operator.id == this.selectedOperator;
  }
}
