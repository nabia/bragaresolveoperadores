import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MapmodalPage } from './mapmodal';
import { AgmCoreModule } from '@agm/core';  

@NgModule({
  declarations: [
    MapmodalPage,
  ],
  imports: [
    IonicPageModule.forChild(MapmodalPage),
    AgmCoreModule 
  ],
})
export class MapmodalPageModule {}
