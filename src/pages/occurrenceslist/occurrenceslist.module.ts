import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OccurrenceslistPage } from './occurrenceslist';

@NgModule({
  declarations: [
    OccurrenceslistPage,
  ],
  imports: [
    IonicPageModule.forChild(OccurrenceslistPage),
  ],
})
export class OccurrenceslistPageModule {}
