import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController } from 'ionic-angular';
import { MapPage } from '../map/map'
import { OccurrencePage } from '../occurrence/occurrence'
import { HistoricPage } from '../historic/historic'
import { ReportPage } from '../report/report'
import { UserservicesProvider } from '../../providers/userservices/userservices';
import { FirebaseProvider } from '../../providers/firebase/firebase';

/**
 * Generated class for the OccurrenceslistPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-occurrenceslist',
  templateUrl: 'occurrenceslist.html',
})
export class OccurrenceslistPage {
  user: any;
  accountId: string;
  occurrences: any;
  occurrencesOnDisplay: any;
  selectedState: number = 0;
  types: string[] = ["Estradas, passeios e outra vias", "Equipamentos públicos", "Iluminação", "Jardins, árvores e outra vegetação", "Limpeza", "Mobiliário urbano", "Trânsito (sinalização vertical e horizontal)", "Outros"];
  constructor( public navCtrl: NavController, public navParams: NavParams, public menu: MenuController, public us: UserservicesProvider, public fb: FirebaseProvider) {
    this.user = this.us.getUser();
    this.accountId = this.us.getAccountId();

    this.fb.getOccurrences().snapshotChanges().map(changes => {
      return changes.map(c => ({ key: c.payload.key, ...c.payload.val() }));
    }).subscribe(result => {
      this.occurrences = {
        awaitingResolution: [],
        beingResolved: [],
        awaitingRevision: [],
        resolved: []
      };
      for(var i = 0; i < result.length; i++){
        if(this.accountId == result[i]["owner"] || result[i]["owner"]  == null || this.user.role == 0){
          switch(result[i]["state"] ){
            case 0: this.occurrences.awaitingResolution.unshift(result[i]); break;
            case 1: this.occurrences.beingResolved.unshift(result[i]); break;
            case 2: this.occurrences.awaitingRevision.unshift(result[i]); break;
            case 3: this.occurrences.resolved.unshift(result[i]); break;
            default: break;
          }
        }
      }
      this.occurrencesOnDisplay = this.occurrences.awaitingResolution;
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OccurrenceslistPage');
  }

  navigateToReport(){
    this.menu.enable(false);
    this.navCtrl.setRoot(ReportPage);
  }

  navigateToMap(){
    this.menu.enable(false);
    this.navCtrl.setRoot(MapPage);
  }

  navigateToHistoric(){
    this.menu.enable(false);
    this.navCtrl.setRoot(HistoricPage);
  }

  logout(){
    this.us.logout();
  }

  setList(state){
    switch(state){
      case 0: 
        this.occurrencesOnDisplay = this.occurrences.awaitingResolution;
        this.selectedState = 0;
        break;
      case 1: 
        this.occurrencesOnDisplay = this.occurrences.beingResolved;
        this.selectedState = 1;
        break;
      case 2: 
        this.occurrencesOnDisplay = this.occurrences.awaitingRevision;
        this.selectedState = 2;
        break;
      case 3: 
        this.occurrencesOnDisplay = this.occurrences.resolved;
        this.selectedState = 3;
        break;
        default: break;                        
    }
  }

  getReadableId(key){
    var hash = key.split("").reduce(function(a,b){a=((a<<5)-a)+b.charCodeAt(0);return a&a},0)
    return hash < 0 ? hash * -1 : hash;
  }

  getReadableType(type){
    return this.types[type];
  }

  navigateToOccurrence(occurrence){
    this.navCtrl.push(OccurrencePage, {
      occurrence: occurrence,
      occurrencesList: this.occurrences.awaitingResolution.concat(this.occurrences.beingResolved.concat(this.occurrences.awaitingRevision).concat(this.occurrences.resolved))
    })
  }
}
