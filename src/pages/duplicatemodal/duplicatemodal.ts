import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ViewController } from 'ionic-angular';
import { UserservicesProvider } from '../../providers/userservices/userservices';
import { OccurrenceservicesProvider } from '../../providers/occurrenceservices/occurrenceservices';
/**
 * Generated class for the DuplicatemodalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-duplicatemodal',
  templateUrl: 'duplicatemodal.html',
})
export class DuplicatemodalPage {
  selectedOccurrence: any;
  selectedOccurrenceId: string;
  occurrences: any[];
  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, public us: UserservicesProvider, public op: OccurrenceservicesProvider) {
    this.occurrences = navParams.get("occurrences");
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DuplicatemodalPage');
  }
  public backModal(){
    this.viewCtrl.dismiss(); 
  }

  public closeModal(){ 
    this.viewCtrl.dismiss({
      occurrence: this.selectedOccurrence
    }); 
  }

  selectOccurrence(occurrence){
    this.selectedOccurrence = occurrence;
    this.selectedOccurrenceId = occurrence.key;
  }

  isSelected(occurrence){
    return occurrence.key == this.selectedOccurrenceId;
  }
}
