import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DuplicatemodalPage } from './duplicatemodal';

@NgModule({
  declarations: [
    DuplicatemodalPage,
  ],
  imports: [
    IonicPageModule.forChild(DuplicatemodalPage),
  ],
})
export class DuplicatemodalPageModule {}
