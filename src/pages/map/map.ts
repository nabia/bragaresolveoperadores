import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import { HttpClient } from '@angular/common/http';
import { Modal, ModalController, ModalOptions } from 'ionic-angular';
import { FirebaseProvider } from '../../providers/firebase/firebase';
import { OccurrenceslistPage } from '../occurrenceslist/occurrenceslist'
import { HistoricPage } from '../historic/historic'
import { ReportPage } from '../report/report'
import { UserservicesProvider } from '../../providers/userservices/userservices';
import { OccurrencePage } from '../occurrence/occurrence'

declare var require: any; 
var firebase = require("firebase"); 

/**
 * Generated class for the MapPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-map',
  templateUrl: 'map.html',
})
export class MapPage {
  lat: number = 41.5454486;
  lng: number = -8.426506999999999;
  localities: any[];
  user: any;
  accountId: string;
  occurrences: any[];
  constructor(public navCtrl: NavController, public navParams: NavParams, public fb: FirebaseProvider, public menu: MenuController, public us: UserservicesProvider, public http: HttpClient) {
    this.fb.getLocalitiesByMunicipality().valueChanges().subscribe(result => {
      this.localities = result;
    });
    this.user = this.us.getUser();
    this.accountId = this.us.getAccountId();
    this.fb.getOccurrences().snapshotChanges().map(changes => {
      return changes.map(c => ({ key: c.payload.key, ...c.payload.val() }));
    }).subscribe(result => {
      var occur = [];
      for(var i = 0; i < result.length; i++){
        if(result[i].state >= 0){
          occur.push(result[i]);
        }
      }
      this.occurrences = occur;
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MapPage');
  }

  navigateToReport(){
    this.menu.enable(false);
    this.navCtrl.setRoot(ReportPage);
  }

  navigateToList(){
    this.menu.enable(false);
    this.navCtrl.setRoot(OccurrenceslistPage);
  }

  navigateToHistoric(){
    this.menu.enable(false);
    this.navCtrl.setRoot(HistoricPage);
  }

  navigateToOccurrence(occurrence){
    this.navCtrl.push(OccurrencePage, {
      occurrence: occurrence,
      occurrencesList: this.occurrences
    })
  }

  selectOnChange(event){
    console.log(event)
    var addressQuery = event + " Braga";
    this.getGeocode(addressQuery).subscribe(result =>{
      if(result["status"] === "OK"){
        console.log(result["results"][0].geometry.location)
        this.lat = result["results"][0].geometry.location.lat;
        this.lng = result["results"][0].geometry.location.lng;
      }
    })
  }

  getGeocode(address){
    var url = "https://maps.googleapis.com/maps/api/geocode/json?address="+address+"&key=AIzaSyDU37wpCZEr54JYeXQMtIokJgOLYu0DJlw";
    return this.http.get(url);
  }
  logout(){
    this.us.logout();
  }
}
