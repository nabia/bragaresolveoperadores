import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController,Platform } from 'ionic-angular';
import { OccurrenceslistPage } from '../occurrenceslist/occurrenceslist'
import { HistoricPage } from '../historic/historic'
import { MapPage } from '../map/map'
import { UserservicesProvider } from '../../providers/userservices/userservices';
import { FormBuilder, Validators } from '@angular/forms';
import { Modal, ModalController } from 'ionic-angular';
import { FirebaseProvider } from '../../providers/firebase/firebase';
import { Diagnostic } from '@ionic-native/diagnostic';
import { Geolocation } from '@ionic-native/geolocation';
import { HttpClient } from '@angular/common/http';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { MapmodalPage } from '../mapmodal/mapmodal';
import { NotificationsserviceProvider } from '../../providers/notificationsservice/notificationsservice';
/**
 * Generated class for the ReportPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
declare var require: any; 
var firebase = require("firebase"); 
declare var window: any;
@IonicPage() 
@Component({
  selector: 'page-report',
  templateUrl: 'report.html',
})
export class ReportPage {
  user: any;
  accountId: string;
  occurrence: any;
  localities: any[];
  lat: number;
  lng: number;
  operators: any[];
  states: any[] = [
    {
      id: 0,
      text: "Por resolver"
    },
    {
      id: 1,
      text: "Atribuída para resolução"
    },
    {
      id: 2,
      text: "Aguarda validação"
    },
    {
      id: 3,
      text: "Resolvida"
    }
  ];
  types: any[] = [
    {
      id: 0,
      text: "Estradas, passeios e outra vias"
    },
    {
      id: 1,
      text: "Equipamentos públicos"
    },
    {
      id: 2,
      text: "Iluminação"
    },
    {
      id: 3,
      text: "Jardins, árvores e outra vegetação"
    },
    {
      id: 4,
      text: "Limpeza"
    },
    {
      id: 5,
      text: "Mobiliário urbano"
    },
    {
      id: 6,
      text: "Trânsito (sinalização vertical e horizontal)"
    },
    {
      id: 7,
      text: "Outros"
    },            
  ];
  base64Image: string;
  uploading: boolean = false;
  reportFailed: boolean = false;
  reportSuccess: boolean = false;
  storeId: string = "289549584765897";
  constructor(public ns: NotificationsserviceProvider, public navCtrl: NavController, public navParams: NavParams, public menu: MenuController, public us: UserservicesProvider, public fbuilder: FormBuilder, public fb: FirebaseProvider, private geolocation: Geolocation, private http: HttpClient,public platform: Platform,  public diagnostic: Diagnostic, public modalCtrl: ModalController, public camera: Camera) {
    this.user = this.us.getUser();
    this.accountId = this.us.getAccountId();

    this.occurrence = fbuilder.group({
      'description' : [null,Validators.compose([Validators.required])],
      'location' : [null,Validators.compose([Validators.required])],
      'address' : [null,Validators.compose([Validators.required])],
      'type' : [null,Validators.compose([Validators.required])],
      'operator' : [null,null],
      'state' : [null,null],
      'file' : [null,null]
    });

    this.fb.getLocalitiesByMunicipality().valueChanges().subscribe(result => {
      this.localities = result;
      this.localities.unshift({
        locality:"Usar posição atual"
      });

      this.localities.unshift({
        locality:"Escolher posição no mapa"
      });
    });
    this.fb.getOperators().snapshotChanges().map(changes => {
      return changes.map(c => ({ key: c.payload.key, ...c.payload.val() }));
    }).subscribe(result => {
      var ops = []
      for(var i = 0; i < result.length; i++){
        if(result[i].activated){
          ops.push(
            {id: result[i]["key"], text: result[i]["firstName"]+" "+result[i]["lastName"]}
          );
        }
      }
      this.operators = ops;
      console.log(this.operators)
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ReportPage');
  }

  navigateToList(){
    this.menu.enable(false);
    this.navCtrl.setRoot(OccurrenceslistPage);
  }

  navigateToMap(){
    this.menu.enable(false);
    this.navCtrl.setRoot(MapPage);
  }

  navigateToHistoric(){
    this.menu.enable(false);
    this.navCtrl.setRoot(HistoricPage);
  }

  logout(){
    this.us.logout();
  }

  openModal(){ 
    const coordsData = {
      lat: 0,
      lng: 0
    }
    const myModal: Modal = this.modalCtrl.create('MapmodalPage', { coords: coordsData });
    myModal.present();
    myModal.onDidDismiss((coords) => {
      if(coords && coords != undefined){
        this.lat = coords.lat;
        this.lng = coords.lng;
        this.parseCoords(coords.lat, coords.lng).subscribe(data => {
          this.occurrence.controls['address'].setValue(data["results"][0].formatted_address);
        });
      }else{
        this.occurrence.controls['location'].setValue(null);
      }

    });
  }

  selectOnChange(event){
    console.log(event)
    if(event === this.localities[0].locality){
      this.openModal();
    }else if(event === this.localities[1].locality){
      if(this.platform.is("android")){
        let successCallback = (isAvailable) => { 
          if(!isAvailable){
            alert("Ligue o GPS para poder obter a sua localização");
          }else{
            this.geolocation.getCurrentPosition().then((resp) => {
              this.lat = resp.coords.latitude;
              this.lng = resp.coords.longitude;
              this.parseCoords(resp.coords.latitude, resp.coords.longitude).subscribe(data => {
                this.occurrence.controls['address'].setValue(data["results"][0].formatted_address);
              });
           }).catch((error) => {
             console.log('Error getting location', error);
           });
          }
        };
        let errorCallback = (e) => console.error(e);
        this.diagnostic.isGpsLocationEnabled().then(successCallback).catch(errorCallback);
      }else{
        this.geolocation.getCurrentPosition().then((resp) => {
          this.lat = resp.coords.latitude;
          this.lng = resp.coords.longitude;
          this.parseCoords(resp.coords.latitude, resp.coords.longitude).subscribe(data => {
            this.occurrence.controls['address'].setValue(data["results"][0].formatted_address);
          });
       }).catch((error) => {
         console.log('Error getting location', error);
         alert("Ligue o GPS para obter a sua localização.");
       });
      }

     }
  }

  parseCoords(lat, lng){
    var url = "https://maps.googleapis.com/maps/api/geocode/json?latlng="+lat+","+lng+"&key=AIzaSyDU37wpCZEr54JYeXQMtIokJgOLYu0DJlw";
    return this.http.get(url);
  }
  getGeocode(address){
    var url = "https://maps.googleapis.com/maps/api/geocode/json?address="+address+"&key=AIzaSyDU37wpCZEr54JYeXQMtIokJgOLYu0DJlw";
    return this.http.get(url);
  }

  logForm(event, value){
    var now = new Date().getTime();
    var occurrence = {
      createdDate: now,
      modifiedDate: now,
      state: 0,
      locality: value.location,
      address: value.address,
      location: null,
      description: value.description,
      media: null,
      mediaType: "image",
      type: value.type,
      createdBy: this.accountId, // insert current user id here
      createdByName: this.user.firstName + " " + this.user.lastName, // current user name here
      owner: null, // insert target operator id here
      ownerName: null, // insert target operator name here
      origin: 0,
      public: false,
      initDate: null,
      endDate: null,
      history: [
        {
          timestamp: now,
          desc: "Criação de ocorrência",
          userId: this.accountId
        }
      ]
    }


    if(value.operator){
      occurrence.state = 1;
      occurrence.owner = value.operator;
      for(var i = 0; i < this.operators.length; i++){
        if(this.operators[i].id == value.operator){
          occurrence.ownerName = this.operators[i].text;
          break;
        }
      }
      occurrence.history.unshift({
        timestamp: now,
        desc: "Atribuíção de operador: " + this.user.firstName + " " + this.user.lastName,
        userId: this.accountId
      });
    }
    
    if(value.state){
      occurrence.state = value.state;
      if(occurrence.state  == 1 ){
        occurrence.initDate = (new Date()).getTime();
      }else if(occurrence.state >= 2){
        occurrence.initDate = (new Date()).getTime();
        occurrence.endDate = (new Date()).getTime();
      }
    }

    if(occurrence.locality != this.localities[0].locality && occurrence.locality != this.localities[1].locality){
      var parsedLocality = occurrence.locality;
      if(occurrence.locality.indexOf("Freguesias") != -1){
        parsedLocality = occurrence.locality.substring(24);
      }
      var addressQuery = occurrence.address+" "+parsedLocality+" Braga";
      this.getGeocode(addressQuery).subscribe(result =>{
        if(result["status"] === "OK"){
          occurrence.location = result["results"][0].geometry.location;
          this.finalizeOccurrence(occurrence);
        }else{
          this.reportFailed = true;
        }
      }
      );
    }else{
      occurrence.location = {
        lat: this.lat,
        lng: this.lng,
      }
      this.finalizeOccurrence(occurrence);
    }

  }

  finalizeOccurrence(occurrence){
    console.log("finalize occurrence called")
    if(this.base64Image){
      var sendOccurrence = this;
      occurrence.mediaType = 1;
      this.uploading = true;
      console.log(JSON.stringify(occurrence))
      this.upload(undefined,function(url) {
        console.log(JSON.stringify(occurrence))
        occurrence.media = url;
        console.log("upload finalized")
        sendOccurrence.fb.addOccurrence(occurrence); 
        sendOccurrence.uploading = false;
        sendOccurrence.reportSuccess = true;
        if(occurrence.owner){
          sendOccurrence.ns.assignOccurrence(occurrence.owner);
        }
      });
    }else{
      console.log(JSON.stringify(occurrence))
      this.fb.addOccurrence(occurrence); 
      if(occurrence.owner){
        this.ns.assignOccurrence(occurrence.owner);
      }
      this.reportSuccess = true;
    }
  }


  fileUpload(){
    const options: CameraOptions = {
      quality: 30,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }

    if(this.platform.is("android")){
      console.log("android")
      options.destinationType= this.camera.DestinationType.DATA_URL;
    }else{
      console.log("other")
    }
    
    this.camera.getPicture(options).then((imageData) => {
      this.base64Image = imageData;
    }, (err) => {
    });
  }

  upload(hasDate,cb) {
    let storageRef = firebase.storage().ref();
    let success = false;
    let date: Date = hasDate != undefined? hasDate : new Date();
    var constructedDate = date.getFullYear().toString() + (date.getMonth()+1).toString() + date.getDate().toString();
    let contentFolder = "images";
    let elem_id = "file";
    let prefixOne = this.storeId;
    let prefixTwo = constructedDate;
    var name = (new Date()).getTime()+".jpeg";
    let path = `/occurrences/${contentFolder}/${prefixTwo}/${prefixOne}/${name}`;
    console.log(path)
    var iRef = storageRef.child(path);

    if(this.platform.is("android")){
      iRef.putString(this.base64Image, 'base64', {contentType:'image/jpeg'}).then((snapshot) => {
        console.log(snapshot.downloadURL)
        cb(snapshot.downloadURL);
      }, (error) => {
        console.log(JSON.stringify(error))
      });
    }else{
      window.resolveLocalFileSystemURL(this.base64Image, (fileEntry) => {
        fileEntry.file((resFile) => {

          var reader = new FileReader();
          reader.onloadend = (evt: any) => {

            var imgBlob: any = new Blob([evt.target.result], { type: 'image/jpeg' });
            iRef.put(imgBlob).then((snapshot) => {
              console.log(snapshot.downloadURL)
              cb(snapshot.downloadURL);
            });
          };
          reader.onerror = (e) => {
            console.log("Failed file read: " + e.toString());
          };
          reader.readAsArrayBuffer(resFile);

        });
      });
    }
  }

  dismissCover(){
    this.reportFailed = false;
  }
}
