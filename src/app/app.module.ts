import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { MapPage } from '../pages/map/map';
import { ReportPage } from '../pages/report/report';
import { HistoricPage } from '../pages/historic/historic';
import { OccurrencePage } from '../pages/occurrence/occurrence';
import { OccurrenceslistPage } from '../pages/occurrenceslist/occurrenceslist';

import { Geolocation } from '@ionic-native/geolocation';
import { AgmCoreModule } from '@agm/core';    
import { Diagnostic } from '@ionic-native/diagnostic';
import { Network } from '@ionic-native/network';
import { File } from '@ionic-native/file';
import { IonicStorageModule } from '@ionic/storage'
import { Camera } from '@ionic-native/camera';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireModule } from 'angularfire2';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { Http,Headers, RequestOptions } from '@angular/http';
import { FirebaseProvider } from '../providers/firebase/firebase'; 
import { AngularFireAuthModule } from 'angularfire2/auth';
import { UserservicesProvider } from '../providers/userservices/userservices';
import { OccurrenceservicesProvider } from '../providers/occurrenceservices/occurrenceservices';
import { NotificationsserviceProvider } from '../providers/notificationsservice/notificationsservice';

const firebaseConfig = { 
  apiKey: "AIzaSyCnK_2bEp9ADimZFZE5euBxG0xoNrzzzBw",
  authDomain: "nabiabot.firebaseapp.com",
  databaseURL: "https://nabiabot.firebaseio.com",
  storageBucket: "nabiabot.appspot.com",
  messagingSenderId: "161378552420"
};    
declare var require: any; 
var firebase = require("firebase"); 
firebase.initializeApp(firebaseConfig);

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    MapPage,
    HistoricPage,
    OccurrencePage,
    OccurrenceslistPage,
    ReportPage
  ],
  schemas:  [ CUSTOM_ELEMENTS_SCHEMA ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    AngularFireDatabaseModule,
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireAuthModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDU37wpCZEr54JYeXQMtIokJgOLYu0DJlw'
    }), 
    HttpClientModule,
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [ 
    MyApp,
    HomePage,
    LoginPage,
    MapPage,
    HistoricPage, 
    OccurrencePage,
    OccurrenceslistPage,
    ReportPage
  ],
  providers: [
    StatusBar,
    SplashScreen, 
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    Geolocation,  
    Camera,
    Diagnostic,
    Network,
    File,
    FirebaseProvider,
    UserservicesProvider,
    OccurrenceservicesProvider,
    NotificationsserviceProvider
  ]
})
export class AppModule {}
