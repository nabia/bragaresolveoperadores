import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { UserservicesProvider } from '../../providers/userservices/userservices';
import { FirebaseProvider } from '../../providers/firebase/firebase';
/*
  Generated class for the OccurrenceservicesProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class OccurrenceservicesProvider {
  types: string[] = ["Estradas, passeios e outra vias", "Equipamentos públicos", "Iluminação", "Jardins, árvores e outra vegetação", "Limpeza", "Mobiliário urbano", "Trânsito (sinalização vertical e horizontal)", "Outros"];
  states: any[] = [
    {
      state: "Ignorada",
      color: "#e73e51"
    },
    {
      state: "Duplicada",
      color: "#D4EF69"
    },
    {
      state: "Por resolver",
      color: "#7f8fa4"
    },
    {
      state: "Atribuída para resolução",
      color: "#0080ff"
    },
    {
      state: "Aguarda validação",
      color: "#000000"
    },
    {
      state: "Resolvida",
      color: "#36931c"
    }    
  ];
  user: any;
  accountId: string;
  constructor(public http: HttpClient, public us: UserservicesProvider, public fb: FirebaseProvider) {
    this.user = this.us.getUser();
    this.accountId = this.us.getAccountId();
  }

  getReadableId(key){
    var hash = key.split("").reduce(function(a,b){a=((a<<5)-a)+b.charCodeAt(0);return a&a},0)
    return hash < 0 ? hash * -1 : hash;
  }

  getReadableType(type){
    return this.types[type];
  }

  saveOccurrence(occurrence, auditEntry){
    var id = occurrence.key;
    var now = (new Date()).getTime(); 
    occurrence.modifiedDate = now;
    occurrence.history.unshift({
      timestamp: now,
      desc: auditEntry,
      userId: this.accountId
    });
    this.fb.setOccurrence(id, occurrence); 
  }
  getReadableState(index){
    var id = index + 2;
    return this.states[id].state;
  }

  getStateColor(index){
    var id = index + 2;
    return this.states[id].color;
  }
}
