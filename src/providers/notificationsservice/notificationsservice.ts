// import { Http, RequestOptions } from '@angular/http';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FirebaseProvider } from '../../providers/firebase/firebase';
/*
  Generated class for the NotificationsserviceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class NotificationsserviceProvider {
  //store: string = "289549584765897";
  store: string = "188196777878184";
  nabiaBotsEndpoint: "https://nabiabots.nabiasolutions.com";
  mailHandlerPort: "3984";
  constructor(public http: HttpClient, public fb: FirebaseProvider) {
    console.log('Hello NotificationsserviceProvider Provider');
  }

  notify(target, origin){
    var or = origin;
    if(origin > 0){
      this.fb.getUser(target).valueChanges().subscribe(result => {
        result.id = target;
        this.finishNotification(result, or);
      });     
    }
  }
  finishNotification(target, origin){
    console.log(target)
    var title = "Ocorrência resolvida";
    var message = "A ocorrência reportada por si já se encontra resolvida. Obrigado"
    switch(origin){
      case 0: break;
      case 1: 
        if(target.facebookId){
          this.sendFBMessage(title, message, target.facebookId)
        } else if(target.id){
          this.sendFBMessage(title, message, target.id)
        }
        break;
      case 2:  
        if(target.deviceToken){
          this.sendNotification(title, message, target.deviceToken)
        }
        break;
      case 3: 
        if(target.deviceToken){
          this.sendNotification(title, message, target.deviceToken)
        }
        break;
      case 4: 
        if(target.email){
          var mail = {
            "to": target.email,
            "subject": "[CM Braga] Ocorrência resolvida",
            "html": "Caro munícipe, <br>"+
            "Serve o presente e-mail para o notificar que a ocorrência que reportou já se encontra resolvida.<br><br>"+
            "Melhores cumprimentos,<br>"+
            "CM Braga"
        };
          this.sendMail(mail, null)
        }else if(target.facebookId){
          this.sendFBMessage(title, message, target.facebookId)
        }
      default: break;
    }
  }

  assignOccurrence(target){
    this.fb.getOperatorById(target).valueChanges().subscribe(result => {
      console.log(result)
      if(result["deviceToken"]){
        this.sendNotification("Atribuição de ocorrência", "Foi-lhe atribuida nova ocorrência para resolução.", result["deviceToken"])
      }else if(result["email"]){
        var mail = {
          "to": result["email"],
          "subject": "[CM Braga] Atribuição de ocorrência",
          "html": "Caro colaborador, <br>"+
          "Foi-lhe atribuida nova ocorrência para resolução, pode visualizar os detalhes da mesma no dashboard de ocorrências.<br>"+
          this.nabiaBotsEndpoint+
          "<br><br>"+
          "Melhores cumprimentos,<br>"+
          "CM Braga"
        };
        this.sendMail(mail, null)
      }
    })
  }

  sendNotification(title, message, deviceId){
    //let headers = new HttpHeaders({'Content-Type': 'application/json', 'Authorization': 'key=AIzaSyDcL5KDVGwk9SxiNva8u8ifS0TqsKs6j6s'});
    let headers = new HttpHeaders({'Content-Type': 'application/json', 'Authorization': 'key=AIzaSyDmgBwrK2ys-6zLHIPSad0qJoEAo3DRxkg'});
    
    let options = {
        headers: headers
    };
    let notification = {
      "to": deviceId, 
      "notification": {
        "title": title,
        "body": message
      }
    };

    this.http.post("https://fcm.googleapis.com/fcm/send", JSON.stringify(notification), options).subscribe(resp => {console.log(resp)});
  }


  sendMail(mail, cb?){
    var mailer = this.nabiaBotsEndpoint+"/"+this.mailHandlerPort+"/sendMail";
    this.http.post(mailer, mail).subscribe(result => {
      if(result["status"] == 200){
        if(cb){
          cb();
        }
      }
    });
  }

  sendFBMessage(title, message, facebookId){
    var port = "3978";
    var payload = {
      "userids": [facebookId],
      "messages": [message],
      "types": [1],
      "date": (new Date()).getTime(),
      "name": title,
      "isTest": false
    }
    let body = JSON.stringify(payload);
    let url = this.nabiaBotsEndpoint+"/api/broadcast";
    let headers = new HttpHeaders({'Content-Type': 'application/json'});
    let options = {
      headers: headers
    };
    this.http.post(url, body, options).subscribe(resp => {console.log(resp)});
  }
}
