import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { SplashScreen } from '@ionic-native/splash-screen';
import { FirebaseProvider } from '../../providers/firebase/firebase';
declare var require: any; 
var firebase = require("firebase"); 
declare var FirebasePlugin: any;
/*
  Generated class for the UserservicesProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UserservicesProvider {
  user: any;
  accountId: string;
  constructor(public http: HttpClient, private storage: Storage, public splashScreen: SplashScreen, public fb: FirebaseProvider) {
  }

  getInitials(name){
    var initials = name.split(" ");
    initials = ((initials.shift()[0] || '') + (initials.pop()[0] || '')).toUpperCase();
    return initials;
  }

  getUserColor(userId){
    if(userId){
      var hash = 0;
      for (var i = 0; i < userId.length; i++) {
          hash = userId.charCodeAt(i) + ((hash << 5) - hash);
      }
      var colour = '#';
      for (var i = 0; i < 3; i++) {
          var value = (hash >> (i * 8)) & 0xFF;
          colour += ('00' + value.toString(16)).substr(-2);
      }
      return colour;   
    }else{
      return "#FFFFFF";
    }
  }

  reloadInfo(hasInfoCB, needsLoginCB){
    this.storage.get('user').then((user) => {
      if(user && user != undefined){
        this.user = JSON.parse(user);
        this.storage.get('accountId').then((id) => {
          if(id && id != undefined){
            this.accountId = JSON.parse(id);
            console.log(this.user);
            console.log(this.accountId);
          }
          console.log("testing for user info");
          if(this.user && this.accountId){
            FirebasePlugin.getToken(token => {
              console.log(token)
              if(token && token != ""){
                this.fb.updateDeviceToken(this.accountId, token);
              }
              FirebasePlugin.subscribe('all');
            }, error => {
              console.error(`Error: ${error}`);
            });
            FirebasePlugin.onTokenRefresh(token => {
              console.log(token);
              if(token && token != ""){
                this.fb.updateDeviceToken(this.accountId, token);
              }
            }, function(error) {
              console.error(`Error: ${error}`);
            });
            hasInfoCB();
          }else{
            needsLoginCB();
          }
        });
      }else{
        needsLoginCB();
      }
    });
  }

  setUser(user, accountId){
    this.user = user;
    this.accountId = accountId;

    this.storage.set('user', JSON.stringify(user));
    this.storage.set('accountId', JSON.stringify(accountId));
    console.log(this.user);
    console.log(this.accountId);
  }

  getUser(){
    return this.user;
  }

  getAccountId(){
    return this.accountId;
  }

  logout(cb?){
    this.storage.set('user', null);
    this.storage.set('accountId', JSON.stringify(null));    
    firebase.auth().signOut();
    if(cb){
      cb();
    }
    this.splashScreen.show();
    location.reload();
  }
}
