import { Injectable } from '@angular/core';
import { AngularFireDatabase} from 'angularfire2/database'; 
import { AngularFireList, AngularFireObject } from 'angularfire2/database/interfaces';
/*
  Generated class for the FirebaseProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class FirebaseProvider { 
  //storeId: string = "289549584765897";
  storeId: string = "188196777878184";
  constructor(public af: AngularFireDatabase) {
  }

  getLocalitiesByMunicipality() : AngularFireList<any[]> {
    return this.af.list('localization/localities',
      ref => ref.orderByChild("municipality").equalTo('braga') 
    );
  }

  getOccurrences() : AngularFireList<any[]> {
    return this.af.list('/merchantstores/'+this.storeId+"/occurrences/");
  }

  getOccurrence(id) : AngularFireObject<any> {
    return this.af.object('/merchantstores/'+this.storeId+"/occurrences/"+id);
  }

  addOccurrence(occurrence) {
    return this.getOccurrences().push(occurrence);
  }

  setOccurrence(id, data) {
    return this.af.object('/merchantstores/'+this.storeId+"/occurrences/"+id).update(data);
  }
  
  getUser(id) : AngularFireObject<any> {
    return this.af.object('/occurrenceUsers/'+this.storeId+'/'+id);
  }

  updateDeviceToken(id, token){
    return this.af.object('/operators/'+this.storeId+"/"+id).update({
      deviceToken: token
    });
  }

  getOperator(id) : AngularFireList<any[]>  {
    console.log("/operators/"+this.storeId);
    return this.af.list("/operators/"+this.storeId,
      ref => ref.orderByChild("accountId").equalTo(id)
    );
  } 

  getOperatorById(id) : AngularFireObject<any> {
    return this.af.object('/operators/'+this.storeId+'/'+id);
  }
  getOperators() : AngularFireList<any[]> {
    return this.af.list('/operators/'+this.storeId);
  }
}
